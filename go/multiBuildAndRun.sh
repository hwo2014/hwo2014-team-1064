#!/bin/bash

USER=`whoami`

./build

if [ "$?" != "0" ]; then
    echo "Build failed"
    exit
fi

BOT_NAME="TOUK-go-$USER"

common_opts="-h hakkinen.helloworldopen.com -k pbmDwxN/bwVA4Q -c 3 -pass 123455"
set -x 
./touk_bot.bin $common_opts -n "${BOT_NAME}_1" -m master 2>&1 | tee /tmp/${BOT_NAME}_1.log &
./touk_bot.bin $common_opts -n "${BOT_NAME}_2" -m join 2>&1 | tee /tmp/${BOT_NAME}_2.log &
./touk_bot.bin $common_opts -n "${BOT_NAME}_3" -m join 2>&1 | tee /tmp/${BOT_NAME}_3.log &
