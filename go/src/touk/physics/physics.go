package physics

import "math"

const (
	//Guess
	GRAVITY = 10.0
	//Effective friction on round pieces, obtained empirically
	FRICTION = 0.048
	//Maximum effective throttle on straight piece with throttle 1.0 sent to server, obtained empirically
	MAX_THROTTLE = 0.12
	//Effective resistance throttle on straight piece with throttle 0.0 sent to server, obtained empirically
	RESISTANCE = -0.015
	INERTIA = 10
)

var Inertia float64 = INERTIA
var Resistance float64 = RESISTANCE

func MaxTurnVelocity(radius float64, gravityAcc float64, friction float64) float64 {
	// weight * V^2 / radius = friction * weight * gravityAcc
	// V = sqrt ( friction * gravityAcc * radius )
	return math.Sqrt(friction * gravityAcc * radius)
}

func CalculateFrictionFromTurnVelocity(radius float64, gravityAcc float64, velocity float64) float64 {
	// weight * V^2 / radius = friction * weight * gravityAcc
	// friction = V^2 / radius * gravityAcc
	return velocity * velocity / (radius * gravityAcc)
}

func DistanceToStartBrakingToMeetMaxTurnVelocity(currentVelocity float64, resistance float64, turnMaxVelocity float64) float64 {
	if turnMaxVelocity >= currentVelocity {
		return 0.0
	}
	//currentVelocity + resistance * time = turnMaxVelocity
	time := (turnMaxVelocity - currentVelocity) / resistance

	//distanceToStartBreaking = resistance * time * time / 2
	distanceToStartBreaking := -1 * resistance * time * time / 2
	return distanceToStartBreaking
}
