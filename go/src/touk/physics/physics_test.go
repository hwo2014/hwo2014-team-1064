package physics

import (
	. "gopkg.in/check.v1"
	"testing"
)

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type PhysicsSuite struct{}

var _ = Suite(&PhysicsSuite{})

func (s *PhysicsSuite) TestShouldCountMaximumVelocity(c *C) {
	//given
	radius := 72.0
	gravity := 10.0
	friction := 0.52

	//when
	velocity := MaxTurnVelocity(radius, gravity, friction)

	//then
	c.Assert(velocity, Equals, 19.349418595916521)
}

func (s *PhysicsSuite) TestShouldCountFrictionFromTurnVelocity(c *C) {
	//given
	radius := 72.0
	gravity := 10.0
	velocity := 19.34941859591652

	//when
	friction := CalculateFrictionFromTurnVelocity(radius, gravity, velocity)

	//then
	c.Assert(friction, Equals, 0.5200000000000001)
}

func (s *PhysicsSuite) TestShouldCountFrictionFromTurnVelocityExperimental1(c *C) {
	//given
	radius := 100.0
	gravity := 10.0
	velocity := 8.5

	//when
	friction := CalculateFrictionFromTurnVelocity(radius, gravity, velocity)

	//then
	c.Assert(friction, Equals, 0.07225)
}

func (s *PhysicsSuite) TestShouldCountMaximumVelocityExperimental1(c *C) {
	//given
	radius := 100.0
	gravity := 10.0
	friction := 0.07225

	//when
	velocity := MaxTurnVelocity(radius, gravity, friction)

	//then
	c.Assert(velocity, Equals, 8.5)
}

func (s *PhysicsSuite) TestShouldCountBrakingDistanceToNextTurn(c *C) {
	//expect
	c.Assert(DistanceToStartBrakingToMeetMaxTurnVelocity(10.0, -0.2, 10.0), Equals, 0.0)
	c.Assert(DistanceToStartBrakingToMeetMaxTurnVelocity(5.0, -0.2, 10.0), Equals, 0.0)
	c.Assert(DistanceToStartBrakingToMeetMaxTurnVelocity(10.0, -0.2, 3.5), Equals, 105.625)
	c.Assert(DistanceToStartBrakingToMeetMaxTurnVelocity(10.0, -0.08, 6), Equals, 100.0)
	c.Assert(DistanceToStartBrakingToMeetMaxTurnVelocity(8.0, -0.08, 6), Equals, 25.0)
}


