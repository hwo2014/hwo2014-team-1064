package model

import (
	"touk/physics"
	"math"
)

type Piece struct {
	//JSON
	Length float64
	Switch bool
	Radius float64
	Angle float64
	//Additional
	MaxVelocity map[int]float64
	Lengths map[int]float64
	EffectiveRadius map[int]float64
}

func (p *Piece) IsTurn() bool {
	return p.Radius != 0.0
}

func (p *Piece) InitConvenientFields(lanes []Lane) {
	if (!p.IsTurn()) {
		p.initLaneLengths(lanes)
		return
	}
	p.CalculateEffectiveRadiuses(lanes)
	p.CalculateMaxVelocity(lanes)
	p.CalculateTurnLengths(lanes)

}

func (p *Piece) CalculateMaxVelocity(lanes []Lane) {
	p.MaxVelocity = make(map[int]float64, 0)
	for _, lane := range lanes {
		p.MaxVelocity[lane.Index] = physics.MaxTurnVelocity(p.EffectiveRadius[lane.Index], physics.GRAVITY, physics.FRICTION)
	}
}

func (p *Piece) CalculateTurnLengths(lanes []Lane) {
	p.Lengths = make(map[int]float64, 0)
	for _, lane := range lanes {
		p.Lengths[lane.Index] = 2 * math.Abs(p.Angle) * math.Pi * p.EffectiveRadius[lane.Index] / 360
	}
}

func (p *Piece) CalculateEffectiveRadiuses(lanes []Lane) {
	p.EffectiveRadius = make(map[int]float64, 0)
	for _, lane := range lanes {
		effectiveRadius := p.Radius
		if p.Angle > 0 {
			effectiveRadius = p.Radius - lane.DistanceFromCenter
		}
		if p.Angle < 0 {
			effectiveRadius = p.Radius + lane.DistanceFromCenter
		}
		p.EffectiveRadius[lane.Index] = effectiveRadius
	}
}

func (p *Piece) initLaneLengths(lanes []Lane) {
	p.Lengths = make(map[int]float64, 0)
	for _, lane := range lanes {
		p.Lengths[lane.Index] = p.Length
	}
}
