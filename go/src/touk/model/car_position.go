package model

type CarPosition struct {
	Id CarId
	Angle float64
	PiecePosition PiecePosition
}

type PiecePosition struct {
	PieceIndex int
	InPieceDistance float64
	Lane LanePosition
	Lap int
}

type LanePosition struct {
	StartLaneIndex int
	EndLaneIndex int
}

