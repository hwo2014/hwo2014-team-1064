package model

type RaceSession struct {
	Laps int
	DurationMs int
	MaxLapTimeMs int
	QuickRace bool
}

type Race struct {
	//from JSON
	Track Track
	Cars []Car
	RaceSession RaceSession

	//additional convenient
	CarMap map[string]*Car
	MyCar *Car
	CarName string
}

func (r *Race) InitConvenientFields() {
	r.Track.InitConvenientFields()
	r.CarMap = make(map[string]*Car, len(r.Cars))
	for idx, car := range r.Cars {
		r.CarMap[car.Id.Name] = &r.Cars[idx]
		if (car.Id.Name == r.CarName) {
			r.MyCar = &r.Cars[idx]
		}
	}
	r.MyCar.SpeedMultiplier = 1
}

func (r *Race) Update(carPositions []CarPosition) {
	for idx, carPosition := range carPositions {
		r.CarMap[carPosition.Id.Name].Update(r, &r.Track, &carPositions[idx])
//		r.CarMap[carPosition.Id.Name].LogSpeeds()
	}
}

func (r *Race) UpdateLap(lapFinished *LapFinished) {
	r.CarMap[lapFinished.Car.Name].UpdateLap(lapFinished)
}

func (r *Race) IsTestRace() (bool) {
	return r.RaceSession.Laps > 0 && r.RaceSession.QuickRace == true
}

func (r *Race) IsRace() (bool) {
	return r.RaceSession.Laps > 0 && r.RaceSession.QuickRace == false
}

func (r *Race) IsQualifying() (bool) {
	return r.RaceSession.DurationMs > 0
}
