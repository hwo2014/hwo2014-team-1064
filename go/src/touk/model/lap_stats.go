package model

type LapStats struct {
	StartTick	int
	EndTick		int
	Ticks		int
	Millis		int
	PieceStats	[]*PieceStats
}

func (ls *LapStats) Length() int {
	return ls.EndTick - ls.StartTick
}

func NewLapStats() *LapStats {
	return &LapStats{PieceStats: make([]*PieceStats, 0)}
}

type PieceStats struct {
	StartTick	int
	EndTick		int
	VMax		float64
	AngleMax	float64
}

func (ps *PieceStats) Length() int {
	return ps.EndTick - ps.StartTick
}

func NewPieceStats() *PieceStats {
	return &PieceStats{}
}
