package model

import (
	"log"
	"touk/physics"
)

type Car struct {
	//JSON
	Id         CarId
	Dimensions CarDimensions

	//additional convenient
	LastPosition *CarPosition
	Stats        *CarStats
	IsTurbo		 bool
	Turbo 		*Turbo
	SpeedMultiplier float64
}

type CarId struct {
	Name  string
	Color string
}

type CarDimensions struct {
	Length float64
	Width  float64
}

func (c *Car) Update(race *Race, track *Track, carPosition *CarPosition) {
	if c.Stats == nil {
		c.Stats = NewCarStats(race)
	}

	c.updateLapStats(race, track, carPosition)

	stats := c.Stats
	nextIdx := stats.PieceIdx + 1
	if nextIdx == len(track.Pieces) {
		nextIdx = 0
	}
	stats.PieceIdx = carPosition.PiecePosition.PieceIndex
	stats.PieceDistance = carPosition.PiecePosition.InPieceDistance
	stats.Piece = &track.Pieces[stats.PieceIdx]
	stats.NextPiece = &track.Pieces[nextIdx]
	stats.LaneIdx = carPosition.PiecePosition.Lane.EndLaneIndex
	stats.Lane = &track.Lanes[stats.LaneIdx]

	if c.LastPosition != nil {
		last := c.LastPosition.PiecePosition
		stats.Distance = track.Distance(last.PieceIndex, last.InPieceDistance, stats.LaneIdx, stats.PieceIdx, stats.PieceDistance)
	} else {
		stats.Distance = stats.PieceDistance
	}

	newVelocity := stats.Distance / 1
	stats.Acceleration = (newVelocity - stats.Velocity) / 1
	stats.Velocity = newVelocity

	newAngleVelocity := (carPosition.Angle - stats.Angle) / 1
	stats.AngleAcceleration = (newAngleVelocity - stats.AngleVelocity) / 1
	stats.AngleVelocity = newAngleVelocity
	stats.Angle = carPosition.Angle
	//if signum(angle acceleration) has different sign than angle
	stats.AnglePeaked = stats.Piece.IsTurn() && stats.Piece.Angle * stats.AngleVelocity < 0

	stats.NextTurnMaxVelocity = track.NextTurnMaxVelocity(stats.PieceIdx, stats.LaneIdx)
	stats.DistanceToNextTurn = track.DistanceToNextTurn(stats.PieceIdx, stats.LaneIdx, stats.PieceDistance)
	stats.DistanceToStartBraking = physics.DistanceToStartBrakingToMeetMaxTurnVelocity(
			stats.Velocity, physics.Resistance, stats.NextTurnMaxVelocity) + physics.Inertia

	c.LastPosition = carPosition
}

func (c *Car) UpdateLap(lapFinished *LapFinished) {
	c.Stats.LapTimes = append(c.Stats.LapTimes, lapFinished.LapTime)
	c.Stats.LapIdx = lapFinished.LapTime.Lap
}

func (c *Car) updateLapStats(race *Race, track *Track, carPosition *CarPosition) {
	if c.Stats.LapStats == nil {
		c.Stats.LapStats = make([]*LapStats, 0)
	}

	if c.Stats.CurrentLapStats == nil {
		c.Stats.CurrentLapStats = NewLapStats()
	} else if c.Stats.LapIdx != carPosition.PiecePosition.Lap {
		c.Stats.CurrentLapStats.EndTick = c.Stats.GameTick - 1
		c.Stats.LapStats = append(c.Stats.LapStats, c.Stats.CurrentLapStats)
		c.Stats.CurrentLapStats = NewLapStats()
		c.Stats.CurrentLapStats.StartTick = c.Stats.GameTick
	}

	if c.Stats.CurrentPieceStats == nil {
		c.Stats.CurrentPieceStats = NewPieceStats()
	} else if c.Stats.PieceIdx != carPosition.PiecePosition.PieceIndex {
		c.Stats.CurrentPieceStats.EndTick = c.Stats.GameTick - 1
		c.Stats.CurrentLapStats.PieceStats = append(c.Stats.CurrentLapStats.PieceStats, c.Stats.CurrentPieceStats)
		c.Stats.CurrentPieceStats = NewPieceStats()
		c.Stats.CurrentPieceStats.StartTick = c.Stats.GameTick
	}
}

func (c *Car) LogSpeeds() {
	log.Printf("Car %v: piece %v distance %v ; sent %v, dist %v, v %v, acc %v, ang %v, ang v %v, ang acc %v",
		c.Id.Name, c.Stats.PieceIdx, c.Stats.PieceDistance, c.Stats.SentOrder, c.Stats.Distance, c.Stats.Velocity,
		c.Stats.Acceleration, c.Stats.Angle, c.Stats.AngleVelocity, c.Stats.AngleAcceleration)

}
