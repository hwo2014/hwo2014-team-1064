package model

import (
	"github.com/mitchellh/mapstructure"
)

func RaceFromJson(json interface{}) *Race {
	var race Race
	err := mapstructure.Decode(json.(map[string]interface{})["race"], &race)
	if err != nil {
		panic(err)
	}
	return &race
}

func CarPositionFromJson(json interface{}) ([]CarPosition) {
	var carPositions []CarPosition
	err := mapstructure.Decode(json.([]interface{}), &carPositions)
	if err != nil {
		panic(err)
	}
	return carPositions
}

func LapFinishedFromJson(json interface{}) *LapFinished {
	var lapFinished LapFinished
	err := mapstructure.Decode(json.(map[string]interface{}), &lapFinished)
	if err != nil {
		panic(err)
	}
	return &lapFinished
}

func TurboFromJson(json interface{}) *Turbo {
	var turbo Turbo
	err := mapstructure.Decode(json.(map[string]interface{}), &turbo)
	if err != nil {
		panic(err)
	}
	return &turbo
}
