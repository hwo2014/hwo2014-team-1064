package model

import (
	. "gopkg.in/check.v1"
)

type RaceSuite struct{}

var _ = Suite(&RaceSuite{})

const TOUK_CAR_NAME = "Touk car"
func (s *RaceSuite) TestShouldInitConvenientFields(c *C) {
	//given
	touk := Car{Id:CarId{Name: TOUK_CAR_NAME}}
	other := Car{Id:CarId{Name: "other"}}
	race := Race{Cars: []Car{touk, other}, CarName: TOUK_CAR_NAME}

	//when
	race.InitConvenientFields()

	//then
	c.Assert(race.MyCar, Equals, &race.Cars[0])
	c.Assert(race.CarMap[TOUK_CAR_NAME], Equals, &race.Cars[0])
	c.Assert(race.CarMap["other"], Equals, &race.Cars[1])
}
