package model

import (
	. "gopkg.in/check.v1"
)

type TrackSuite struct{}

var _ = Suite(&TrackSuite{})

func (s *TrackSuite) TestShouldCalculateDistance(c *C) {
	//given
	lanes := []Lane{Lane{DistanceFromCenter: -10.0, Index: 0}, Lane{DistanceFromCenter: 10.0, Index: 1}}
	pieces := []Piece{Piece{Length: 100.0}}
	track := Track{Pieces: pieces}
	for i := 0; i < len(pieces); i++ {
		piece := &pieces[i]
		piece.InitConvenientFields(lanes)
	}

	//when
	distance := track.Distance(0, 20.0, 0, 0, 60.0)

	//then
	c.Assert(distance, Equals, 40.0)
}

func (s *TrackSuite) TestShouldCalculateDistanceWithDifferentPieces(c *C) {
	//given
	lanes := []Lane{Lane{DistanceFromCenter: -10.0, Index: 0}, Lane{DistanceFromCenter: 10.0, Index: 1}}
	pieces := []Piece{Piece{Length: 100.0}, Piece{Length: 80.0}}
	track := Track{Pieces: pieces}
	for i := 0; i < len(pieces); i++ {
		piece := &pieces[i]
		piece.InitConvenientFields(lanes)
	}

	//when
	distance := track.Distance(0, 98.0, 0, 1, 7.0)

	//then
	c.Assert(distance, Equals, 9.0)
}

func (s *TrackSuite) TestShouldCalculateDistanceWithCyclicPieces(c *C) {
	//given
	lanes := []Lane{Lane{DistanceFromCenter: -10.0, Index: 0}, Lane{DistanceFromCenter: 10.0, Index: 1}}
	pieces := []Piece{Piece{Length: 100.0}, Piece{Length: 80.0}}
	track := Track{Pieces: pieces}
	for i := 0; i < len(pieces); i++ {
		piece := &pieces[i]
		piece.InitConvenientFields(lanes)
	}

	//when
	distance := track.Distance(1, 20.0, 0, 0, 60.0)

	//then
	c.Assert(distance, Equals, 120.0)
}

func (s *TrackSuite) TestShouldCalculateDistanceToNextTurn(c *C) {
	//given
	lanes := []Lane{Lane{DistanceFromCenter: -10.0, Index: 0}, Lane{DistanceFromCenter: 10.0, Index: 1}}
	pieces := []Piece{Piece{Length: 100.0}, Piece{Length: 80.0}, Piece{Angle: 45.0, Radius: 45.0}, Piece{Length: 30}}
	track := Track{Pieces: pieces}
	for i := 0; i < len(pieces); i++ {
		piece := &pieces[i]
		piece.InitConvenientFields(lanes)
	}

	//expect
	c.Assert(track.DistanceToNextTurn(0, 0, 50.0), Equals, 130.0)
	c.Assert(track.DistanceToNextTurn(1, 1, 60.0), Equals, 20.0)
	c.Assert(track.DistanceToNextTurn(3, 0, 10.0), Equals, 200.0)
}

func (s *TrackSuite) TestShouldFindNextTurn(c *C) {
	//given
	turn1 := Piece{Radius: 45.0}
	turn2 := Piece{Radius: -45.0}
	pieces := []Piece{Piece{Length: 100.0}, Piece{Length: 80.0}, turn1, Piece{Length: 30}, turn2}
	track := Track{Pieces: pieces}

	//expect
	_, nextTurn := track.NextTurn(0)
	c.Assert(nextTurn, Equals, &track.Pieces[2])
	_, nextTurn = track.NextTurn(1)
	c.Assert(nextTurn, Equals, &track.Pieces[2])
	_, nextTurn = track.NextTurn(2)
	c.Assert(nextTurn, Equals, &track.Pieces[4])
	_, nextTurn = track.NextTurn(4)
	c.Assert(nextTurn, Equals, &track.Pieces[2])
}
