package model

import (
	"testing"
	"encoding/json"
	. "gopkg.in/check.v1"
)

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type JsonSuite struct{}

var _ = Suite(&JsonSuite{})

func (s *JsonSuite) TestShouldParseRace(c *C) {
	//given
	var msg interface{}
	json.Unmarshal([]byte(JSON_GAME_INIT), &msg)

	//when
	race := RaceFromJson(msg)

	//then
	c.Assert(race.IsRace(), Equals, false)
	c.Assert(race.Track.Id, Equals, "indianapolis")
	c.Assert(race.Track.Name, Equals, "Indianapolis")

	c.Assert(len(race.Track.Pieces), Equals, 3)
	c.Assert(race.Track.Pieces[0].Length, Equals, 100.0)
	c.Assert(race.Track.Pieces[0].Switch, Equals, false)
	c.Assert(race.Track.Pieces[0].Radius, Equals, 0.0)
	c.Assert(race.Track.Pieces[0].Angle, Equals, 0.0)
	c.Assert(race.Track.Pieces[1].Length, Equals, 100.0)
	c.Assert(race.Track.Pieces[1].Switch, Equals, true)
	c.Assert(race.Track.Pieces[2].Length, Equals, 0.0)
	c.Assert(race.Track.Pieces[2].Switch, Equals, false)
	c.Assert(race.Track.Pieces[2].Radius, Equals, 200.0)
	c.Assert(race.Track.Pieces[2].Angle, Equals, 22.5)

	c.Assert(len(race.Track.Lanes), Equals, 3)
	c.Assert(race.Track.Lanes[0].DistanceFromCenter, Equals, -20.0)
	c.Assert(race.Track.Lanes[0].Index, Equals, 0)

	c.Assert(len(race.Cars), Equals, 2)
	c.Assert(race.Cars[0].Id.Name, Equals, "Schumacher")
	c.Assert(race.Cars[0].Id.Color, Equals, "red")
	c.Assert(race.Cars[0].Dimensions.Length, Equals, 40.0)
	c.Assert(race.Cars[0].Dimensions.Width, Equals, 20.0)
}

func (s *JsonSuite) TestShouldParseRealRace(c *C) {
	//given
	var msg interface{}
	json.Unmarshal([]byte(JSON_RACE_GAME_INIT), &msg)

	//when
	race := RaceFromJson(msg)

	//then
	c.Assert(race.IsRace(), Equals, true)
	c.Assert(race.Track.Id, Equals, "indianapolis")
	c.Assert(race.Track.Name, Equals, "Indianapolis")
}

func (s *JsonSuite) TestShouldParseCarPositions(c *C) {
	//given
	var msg interface{}
	json.Unmarshal([]byte(JSON_CAR_POSITIONS), &msg)

	//when
	carPositions := CarPositionFromJson(msg)

	//then
	c.Assert(len(carPositions), Equals, 2)
	c.Assert(carPositions[1].Id.Name, Equals, "Rosberg")
	c.Assert(carPositions[1].Angle, Equals, 45.0)
	c.Assert(carPositions[1].PiecePosition.PieceIndex, Equals, 1)
	c.Assert(carPositions[1].PiecePosition.InPieceDistance, Equals, 20.0)
	c.Assert(carPositions[1].PiecePosition.Lane.StartLaneIndex, Equals, 1)
	c.Assert(carPositions[1].PiecePosition.Lane.EndLaneIndex, Equals, 1)
}

func (s *JsonSuite) TestShouldParseLapFinished(c *C) {
	//given
	var msg interface{}
	json.Unmarshal([]byte(JSON_LAP_FINISHED), &msg)

	//when
	lapFinished := LapFinishedFromJson(msg)

	//then
	c.Assert(lapFinished.Car.Name, Equals, "Schumacher")
	c.Assert(lapFinished.LapTime.Lap, Equals, 1)
	c.Assert(lapFinished.LapTime.Ticks, Equals, 666)
	c.Assert(lapFinished.LapTime.Millis, Equals, 6660)
}

const JSON_GAME_INIT = `
{"race": {
    "track": {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ],
      "startingPoint": {
        "position": {
          "x": -340.0,
          "y": -96.0
        },
        "angle": 90.0
      }
    },
    "cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ],
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
  }
}`

const JSON_RACE_GAME_INIT = `
{"race": {
    "track": {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ],
      "startingPoint": {
        "position": {
          "x": -340.0,
          "y": -96.0
        },
        "angle": 90.0
      }
    },
    "cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ],
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": false
    }
  }
}`

const JSON_CAR_POSITIONS = `
[
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 1,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
]
`

const JSON_LAP_FINISHED = `
{
  "car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 666,
    "millis": 6660
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }
}
`
