package model

type CarStats struct {
	LapIdx          int
	PieceIdx        int
	Piece         *Piece
	NextPiece     *Piece
	PieceDistance   float64
	LaneIdx         int
	Lane          *Lane

	LapTimes []*LapTime
	LapStats []*LapStats
	CurrentLapStats *LapStats
	CurrentPieceStats *PieceStats

	DistanceToNextTurn     float64
	NextTurnMaxVelocity    float64
	DistanceToStartBraking float64

	SentOrder         interface{}
	LastSwitch        interface{}
	Acceleration      float64
	Velocity          float64
	Distance          float64
	Angle             float64
	AngleVelocity     float64
	AngleAcceleration float64
	AnglePeaked       bool
	GameTick          int
}

func NewCarStats(race *Race) *CarStats {
	return &CarStats{LapIdx:0, LapTimes:make([]*LapTime, race.RaceSession.Laps), LapStats:make([]*LapStats, 0)}
}
