package model

type Turbo struct {
	TurboDurationMilliseconds float64
	TurboDurationTicks int
	TurboFactor float64
}
