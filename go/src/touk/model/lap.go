package model

type LapFinished struct {
	LapTime *LapTime
	Car *CarId
}

type LapTime struct {
	Lap int
	Ticks int
	Millis int
}
