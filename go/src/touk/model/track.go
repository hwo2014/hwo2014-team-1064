package model

type Track struct {
	Id     string
	Name   string
	Pieces []Piece
	Lanes  []Lane
}

func (t *Track) InitConvenientFields() {
	for idx, _ := range t.Pieces {
		t.Pieces[idx].InitConvenientFields(t.Lanes)
	}
}

func (t *Track) Distance(startPieceIndex int, startPieceDistance float64, startPieceLaneIndex int, endPieceIndex int, endPieceDistance float64) float64 {
	if (endPieceIndex == startPieceIndex) {
		return float64(endPieceDistance - startPieceDistance)
	}
	sum := t.Pieces[startPieceIndex].Lengths[startPieceLaneIndex] - startPieceDistance
	for idx, piece := range t.Pieces[startPieceIndex + 1:len(t.Pieces)] {
		if (idx + startPieceIndex + 1 == endPieceIndex) {
			return sum + endPieceDistance
		}
		sum += piece.Lengths[startPieceLaneIndex]
	}
	for idx, piece := range t.Pieces[0:startPieceIndex] {
		if (idx == endPieceIndex) {
			return sum + endPieceDistance
		}
		sum += piece.Lengths[startPieceLaneIndex]
	}
	return sum
}

func (t *Track) DistanceToNextTurn(currentPieceIndex int, currentLaneIndex int, currentPieceDistance float64) float64 {
	sum := t.Pieces[currentPieceIndex].Lengths[currentLaneIndex] - currentPieceDistance
	for _, piece := range t.Pieces[currentPieceIndex + 1:len(t.Pieces)] {
		if (piece.IsTurn()) {
			return sum
		}
		sum += piece.Lengths[currentLaneIndex]
	}
	for _, piece := range t.Pieces[0:currentPieceIndex] {
		if (piece.IsTurn()) {
			return sum
		}
		sum += piece.Lengths[currentLaneIndex]
	}
	return sum
}

func (t *Track) NextTurnMaxVelocity(currentPieceIndex int, currentLaneIndex int) float64 {
	_, nextTurn := t.NextTurn(currentPieceIndex)
	return nextTurn.MaxVelocity[currentLaneIndex]
}

func (t *Track) NextTurn(currentPieceIndex int) (int, *Piece) {
	for idx, piece := range t.Pieces[currentPieceIndex + 1:len(t.Pieces)] {
		if (piece.IsTurn()) {
			return currentPieceIndex + 1 + idx, &t.Pieces[currentPieceIndex + 1 + idx]
		}
	}
	for idx, piece := range t.Pieces[0:currentPieceIndex] {
		if (piece.IsTurn()) {
			return idx, &t.Pieces[idx]
		}
	}
	return -1, nil
}

func (t *Track) NextSwitch(currentPieceIndex int) (int, *Piece) {
	for idx, piece := range t.Pieces[currentPieceIndex + 1:len(t.Pieces)] {
		if (piece.Switch) {
			return currentPieceIndex + 1 + idx, &t.Pieces[currentPieceIndex + 1 + idx]
		}
	}
	for idx, piece := range t.Pieces[0:currentPieceIndex] {
		if (piece.Switch) {
			return idx, &t.Pieces[idx]
		}
	}
	return -1, nil
}
