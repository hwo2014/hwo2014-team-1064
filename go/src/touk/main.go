package main

import (
	"strings"
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"touk/ai"
	"touk/model"
	"flag"
	"touk/physics"
)

var (
	race            *model.Race
	csvLog          *log.Logger
	laneVisitor     = &ai.LaneByDistanceVisitor{}
	throttleVisitor = &ai.ThrottleVisitor{}
	should_send_ping = false
)

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_create_race(writer *bufio.Writer, name, key, track, password string, numCars int) (err error) {
	data := custom_race_helper(name, key, track, password, numCars)
	log.Printf("Create race %v", data)
	err = write_msg(writer, "createRace", data)
	return
}

func custom_race_helper(name, key, track, password string, numCars int) (map[string]interface{}) {
	botId := make(map[string]string)
	botId["name"] = name
	botId["key"] = key
	data := make(map[string]interface{})
	data["botId"] = botId
	data["trackName"] = track
	if password != "" {
		data["password"] = password
	}
	data["carCount"] = numCars
	return data
}

func send_join(writer *bufio.Writer, name, key string) (err error) {
	data := make(map[string]interface{})
	data["name"] = name
	data["key"] = key
	log.Printf("Join %v", data)
	err = write_msg(writer, "join", data)
	return
}

func send_join_race(writer *bufio.Writer, name, key, track, password string, numCars int) (err error) {
	data := custom_race_helper(name, key, track, password, numCars)
	log.Printf("Join race %v", data)
	err = write_msg(writer, "joinRace", data)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	if should_send_ping {
		err = write_msg(writer, "ping", make(map[string]string))
	} else {
		err = nil
	}
	return
}

func send_throttle(writer *bufio.Writer, throttle float64) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}

func send_turbo(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "turbo", "All hell breaks loose")
	return
}

func send_switch(writer *bufio.Writer, direction string) (err error) {
	err = write_msg(writer, "switchLane", direction)
	return
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}, carName string, gameTick int) (err error) {
	switch msgtype {
	case "join":
		log.Printf("Joined")
		send_ping(writer)
	case "gameInit":
		log.Printf("Game init: %v", data)
		race = model.RaceFromJson(data)
		race.CarName = carName
		race.InitConvenientFields()
		log.Printf("Race %v quals %v test %v", race.IsRace(), race.IsQualifying(), race.IsTestRace())
		send_ping(writer)
	case "gameStart":
		log.Printf("Game started")
		send_ping(writer)
	case "gameEnd":
		log.Printf("Game ended")
		for _, lapTime := range race.MyCar.Stats.LapTimes {
			log.Printf("%v", lapTime)
		}
		send_ping(writer)
	case "crash":
		log.Printf("Crashed on piece %v", race.MyCar.Stats.PieceIdx)
		send_ping(writer)
	case "carPositions":
		carPositions := model.CarPositionFromJson(data)
		race.Update(carPositions)
		race.MyCar.Stats.GameTick = gameTick

		if laneOrder := laneVisitor.Visit(race, race.MyCar.Stats); laneOrder != ai.SWITCH_NONE && laneOrder != race.MyCar.Stats.LastSwitch {
			race.MyCar.Stats.SentOrder = laneOrder
			race.MyCar.Stats.LastSwitch = laneOrder
			csvStats(race.MyCar.Stats)
			send_switch(writer, laneOrder.Lane)
			return
		}

		throttleOrder := throttleVisitor.Visit(race, race.MyCar.Stats)
		if throttleOrder.Turbo {
			log.Printf("Using Turbo!!!")
			send_turbo(writer)
		}
		race.MyCar.Stats.SentOrder = throttleOrder
		csvStats(race.MyCar.Stats)
		send_throttle(writer, throttleOrder.Throttle)
	case "lapFinished":
		lapFinished := model.LapFinishedFromJson(data)
		race.UpdateLap(lapFinished)
		log.Printf("Car %v has finished %v lap with time %v (%v ticks)", lapFinished.Car.Name, lapFinished.LapTime.Lap, lapFinished.LapTime.Millis, lapFinished.LapTime.Ticks)
		send_ping(writer)
	case "turboAvailable":
		log.Printf("Turbo available!")
		race.MyCar.IsTurbo = true
		race.MyCar.Turbo = model.TurboFromJson(data)
		send_ping(writer)
	case "turboStart":
		log.Printf("Turbo start on piece %v with velocity %v and multiplier %v", race.MyCar.Stats.PieceIdx, race.MyCar.Stats.Velocity, race.MyCar.Turbo.TurboFactor)
		race.MyCar.SpeedMultiplier = race.MyCar.Turbo.TurboFactor
	case "turboEnd":
		log.Printf("Turbo end on piece %v with velocity %v", race.MyCar.Stats.PieceIdx, race.MyCar.Stats.Velocity)
		race.MyCar.SpeedMultiplier = 1
	case "tournamentEnd":
		log.Printf("Tournament has ended")
//		csvLapStats(race, race.MyCar.Stats)
//		logLapStats(race, race.MyCar.Stats)
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", data))
		send_ping(writer)
	default:
		log.Printf("Got msg type: %s", msgtype)
		send_ping(writer)
	}
	return
}

func csvHeader() {
	csvLog.Printf("Game tick|Piece index|Piece distance|Lane|Sent order|Velocity|Acceleration|Angle|Angle v|Angle acc|Angle peaked|Next turn max velocity|Distance to next turn|Distance to start braking|Piece is turn|Piece length|Piece angle|Piece radius|Piece max velocity|")
}

func csvStats(stats *model.CarStats) {
	csvLog.Printf("%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|",
		stats.GameTick,
		stats.PieceIdx,
		stats.PieceDistance,
		stats.LaneIdx,
		stats.SentOrder,
		stats.Velocity,
		stats.Acceleration,
		stats.Angle,
		stats.AngleVelocity,
		stats.AngleAcceleration,
		stats.AnglePeaked,
		stats.NextTurnMaxVelocity,
		stats.DistanceToNextTurn,
		stats.DistanceToStartBraking,
		stats.Piece.IsTurn(),
		stats.Piece.Lengths,
		stats.Piece.Angle,
		stats.Piece.EffectiveRadius,
		stats.Piece.MaxVelocity)
	
	log.Printf("%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|%v|",
		stats.GameTick,
		stats.PieceIdx,
		stats.PieceDistance,
		stats.LaneIdx,
		stats.SentOrder,
		stats.Velocity,
		stats.Acceleration,
		stats.Angle,
		stats.AngleVelocity,
		stats.AngleAcceleration,
		stats.AnglePeaked,
		stats.NextTurnMaxVelocity,
		stats.DistanceToNextTurn,
		stats.DistanceToStartBraking,
		stats.Piece.IsTurn(),
		stats.Piece.Lengths,
		stats.Piece.Angle,
		stats.Piece.EffectiveRadius,
		stats.Piece.MaxVelocity)
}

func csvLapStats(race *model.Race, stats *model.CarStats) {
	csvLog.Printf("")
	laps := ""
	for idx, _ := range stats.LapStats {
		laps = fmt.Sprintf("%v|Lap %v", laps, idx + 1)
	}
	csvLog.Printf("Piece|Piece angle%v", laps)
	for pieceIdx, piece := range race.Track.Pieces {
		line := fmt.Sprintf("%v|%v", pieceIdx, piece.Angle)
		for _, lapStats := range stats.LapStats {
			if len(lapStats.PieceStats) > pieceIdx {
				line = fmt.Sprintf("%v|%v", line, lapStats.PieceStats[pieceIdx].Length())
			}
		}
		csvLog.Printf(line)
	}
	line := "Total|"
	for _, lapStats := range stats.LapStats {
		line = fmt.Sprintf("%v|%v", line, lapStats.Length())
	}
	csvLog.Printf(line)
}

func logLapStats(race *model.Race, stats *model.CarStats) {
	laps := "Pcs\tAng"
	for idx, _ := range stats.LapStats {
		laps = fmt.Sprintf("%v\tL%v", laps, idx + 1)
	}
	log.Printf(laps)
	for pieceIdx, piece := range race.Track.Pieces {
		line := fmt.Sprintf("%v\t%v", pieceIdx, piece.Angle)
		for _, lapStats := range stats.LapStats {
			if len(lapStats.PieceStats) > pieceIdx {
				line = fmt.Sprintf("%v\t%v", line, lapStats.PieceStats[pieceIdx].Length())
			}
		}
		log.Printf(line)
	}
	line := "Total"
	for _, lapStats := range stats.LapStats {
		line = fmt.Sprintf("%v\t%v", line, lapStats.Length())
	}
	log.Printf(line)
}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}, carName string) (err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}

		gameTick, _ := msg["gameTick"].(float64)
		switch msg["data"].(type) {
		default:
			err = dispatch_msg(writer, msg["msgType"].(string), nil, carName, int(gameTick))
			if err != nil {
				return
			}
		case interface{}:
			err = dispatch_msg(writer, msg["msgType"].(string), msg["data"].(interface{}), carName, int(gameTick)	)
			if err != nil {
				return
			}
		}
	}
	return
}

func bot_loop(conn net.Conn, name, key, track, password string, numCars int) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	if strings.HasPrefix(name, "ci") {
		send_join(writer, name, key)
	} else {
		send_join_race(writer, name, key, track, password, numCars)
	}
	input, err := read_msg(reader)
	if err != nil {
		log.Print(input)
		log_and_exit(err)
	}
	for {
		input, err := read_msg(reader)
		if err != nil {
			log.Printf("Error read_msg")
			log_and_exit(err)
		}
		err = parse_and_dispatch_input(writer, input, name)
		if err != nil {
			log.Printf("Error parse_and_dispatch_input")
			log_and_exit(err)
		}
	}
}

func parse_args() (host string, port int, name, key, track string, numCars int, password string, inertia float64, resistance float64) {

	flag.StringVar(&host, "h", "gameserver", "server hostname")
	flag.IntVar(&port, "p", 8091, "server port")
	flag.StringVar(&name, "n", "Julian", "Bot name")
	flag.StringVar(&key, "k", "????", "auth key")
	flag.StringVar(&track, "t", "keimola", "track name")
	flag.Float64Var(&inertia, "i", physics.INERTIA, "interia")
	flag.Float64Var(&resistance, "r", physics.RESISTANCE, "resistance")
	flag.BoolVar(&should_send_ping, "ping", false, "send pings to server (not needed in real race)")

	flag.IntVar(&numCars, "c", 1, "run race for more cars")
	flag.StringVar(&password, "pass", "", "for multirace - common password")

	flag.Parse()

	if key == "????" || host == "gameserver" {
		usage()
	}

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func usage() {
	// Fprintf allows us to print to a specifed file handle or stream
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	// PrintDefaults() may not be exactly what we want, but it could be
	flag.PrintDefaults()
	os.Exit(1)
}

func main() {

	host, port, name, key, track, numCars, password, inertia, resistance := parse_args()

	physics.Inertia = inertia
	physics.Resistance = resistance

	fmt.Printf("Physics: inertia %v, resistance %v\n", inertia, resistance)
	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	csv, err := os.Create("./race.csv")
	if err != nil {
		log_and_exit(err)
	}
	csvLog = log.New(csv, "", 0)
	csvHeader()

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	log.Println("Connected")

	defer csv.Close()
	defer conn.Close()

	err = bot_loop(conn, name, key, track, password, numCars)
}
