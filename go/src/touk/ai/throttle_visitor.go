package ai

import (
	"touk/model"
	"math"
)

type ThrottleVisitor struct {
}

type ThrottleOrder struct {
	Throttle float64
	Turbo	 bool
	Reason   string
}

var THROTTLE_ZERO = 0.0
var THROTTLE_ONE = 1.0
var TURBO_SAFE_DISTANCE = 300.0

func (tv *ThrottleVisitor) Visit(race *model.Race, stats *model.CarStats) ThrottleOrder {
	if stats.Piece.IsTurn() {
		return tv.visitTurn(race, stats)
	}
	return tv.visitStraight(race, stats)
}

func (tv *ThrottleVisitor) visitStraight(race *model.Race, stats *model.CarStats) ThrottleOrder {
	currentVelocity := race.MyCar.SpeedMultiplier * stats.Velocity

	//Ugly way to detect we're on turbo
	if race.MyCar.SpeedMultiplier > 1 {
		return ThrottleOrder{Throttle:THROTTLE_ONE, Reason:"turbo baby!!!", Turbo: false}

	}

	if stats.DistanceToNextTurn <= stats.DistanceToStartBraking &&
		currentVelocity > stats.NextTurnMaxVelocity {
		return ThrottleOrder{Throttle:THROTTLE_ZERO, Reason: "before turn and velocity > max", Turbo: false}
	}

	return ThrottleOrder{Throttle:THROTTLE_ONE, Reason:"default", Turbo: canUseTurbo(race, stats)}

}

func (tv *ThrottleVisitor) visitTurn(race *model.Race, stats *model.CarStats) ThrottleOrder {
//	nextTurnIndex, _ := race.Track.NextTurn(stats.PieceIdx)
//	nextIsTurn := nextTurnIndex == stats.PieceIdx+1
	currentVelocity := race.MyCar.SpeedMultiplier * stats.Velocity

	if (math.Abs(stats.AngleVelocity) > 3.2 && math.Abs(stats.Angle) > 35) {
		return ThrottleOrder{Throttle:THROTTLE_ZERO, Reason: "turn chill down ang v", Turbo: false}
	}
/*
	//be polite, don't speed just yet
	if nextIsTurn {
		if stats.Velocity > stats.Piece.MaxVelocity[stats.LaneIdx] {
			return ThrottleOrder{Throttle:THROTTLE_ZERO, Reason: "turn down to vmax", Turbo: false}
		} else {
			return ThrottleOrder{Throttle:THROTTLE_ONE, Reason: "turn up to vmax", Turbo: false}
		}
	}*/

	//maybe we can speed up for this turn but there is another turn next
	if (stats.DistanceToNextTurn <= stats.DistanceToStartBraking &&
		currentVelocity > stats.NextTurnMaxVelocity) {
		return ThrottleOrder{Throttle:THROTTLE_ZERO, Reason: "next turn is harder", Turbo: false}
	}

	//if we're too fast
	if currentVelocity > stats.Piece.MaxVelocity[stats.LaneIdx] {
		if (stats.AnglePeaked) {
			return ThrottleOrder{Throttle:THROTTLE_ONE, Reason: "last turn angle peaked!", Turbo: false}
		}
		return ThrottleOrder{Throttle:THROTTLE_ZERO, Reason: "last turn down to vmax", Turbo: false}
	}

	if (stats.Angle > 50) {
		if (stats.AnglePeaked) {
			return ThrottleOrder{Throttle:THROTTLE_ONE, Reason: "last turn angle 50+ but peaked!", Turbo: false}
		}
		return ThrottleOrder{Throttle:THROTTLE_ZERO, Reason: "last turn but 50+", Turbo: false}
	} else {
		throttle := THROTTLE_ONE
		if race.MyCar.SpeedMultiplier > 1 {
			throttle = throttle/3
		}
		return ThrottleOrder{Throttle:THROTTLE_ONE, Reason: "last turn up to vmax", Turbo: false}
	}

	return ThrottleOrder{Throttle:THROTTLE_ONE, Reason:"turn should not reach", Turbo: false}
}

func canUseTurbo(race *model.Race, stats *model.CarStats) bool {
	if !race.MyCar.IsTurbo {
		return false
	}

	nextTurnIndex, _ := race.Track.NextTurn(stats.PieceIdx)
	distance := race.Track.Distance(stats.PieceIdx, stats.PieceDistance, stats.LaneIdx, nextTurnIndex, 0.0)
	if distance > TURBO_SAFE_DISTANCE {
		race.MyCar.IsTurbo = false
		race.MyCar.SpeedMultiplier = race.MyCar.Turbo.TurboFactor
		return true
	}
	return false
}
