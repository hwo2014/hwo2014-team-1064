package ai

import "touk/model"

type LaneVisitor struct {
}

type LaneOrder struct {
	SwitchIdx int
	Lane string
}

var SWITCH_NONE = LaneOrder{}
var SWITCH_LEFT = LaneOrder{Lane: "Left"}
var SWITCH_RIGHT = LaneOrder{Lane: "Right"}

func (lv *LaneVisitor) Visit(race *model.Race, stats *model.CarStats) LaneOrder {
	turn_idx, _ := race.Track.NextTurn(race.MyCar.LastPosition.PiecePosition.PieceIndex)
	switch_idx, _ := race.Track.NextSwitch(race.MyCar.LastPosition.PiecePosition.PieceIndex)
	next_switch_idx, _ := race.Track.NextSwitch(switch_idx)

	if race.MyCar.Stats.Velocity == 0 {
		return SWITCH_NONE
	}
	if turn_idx < switch_idx {
		return SWITCH_NONE
	}

	currentLane := race.MyCar.LastPosition.PiecePosition.Lane.StartLaneIndex
	angle := 0.0
	distance := make(map[int]float64, 0)
	for _, lane := range race.Track.Lanes {
		distance[lane.Index] = 0.0
	}
	for i:= switch_idx; i < next_switch_idx; i++ {
		angle += race.Track.Pieces[i].Angle
		for _, lane := range race.Track.Lanes {
			distance[lane.Index] += race.Track.Pieces[i].Lengths[lane.Index]
		}
	}

	if angle > 0 && currentLane < len(race.Track.Lanes)-1 {
		return LaneOrder{Lane: "Right", SwitchIdx: switch_idx}
	}
	if angle < 0 && currentLane > 0 {
		return LaneOrder{Lane: "Left", SwitchIdx: switch_idx}
	}
	return SWITCH_NONE
}
