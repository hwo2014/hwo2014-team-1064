package ai

import (
	"touk/model"
)

type LaneByDistanceVisitor struct {
}

func (lv *LaneByDistanceVisitor) Visit(race *model.Race, stats *model.CarStats) LaneOrder {
	switch_idx, _ := race.Track.NextSwitch(race.MyCar.LastPosition.PiecePosition.PieceIndex)
	next_switch_idx, _ := race.Track.NextSwitch(switch_idx)

	if race.MyCar.Stats.Velocity == 0 {
		return SWITCH_NONE
	}

	distances := make(map[int]float64, 0)
	for _, lane := range race.Track.Lanes {
		distances[lane.Index] = race.Track.Distance(switch_idx, 0.0, lane.Index, next_switch_idx, 0.0)
	}

	leastDistance := 9999.0
	leastIndex := 0
	for _, lane := range race.Track.Lanes {
		if distances[lane.Index] < leastDistance {
			leastDistance, leastIndex = distances[lane.Index], lane.Index
		}
	}

	distancesEqual := true;
	for _, lane := range race.Track.Lanes {
		distancesEqual = distancesEqual && distances[lane.Index] == leastDistance

	}

//	log.Printf("Distances %v, chosen %v, current lane %v, equal? %v", distances, leastIndex, stats.LaneIdx, distancesEqual)

	if distancesEqual {
		return SWITCH_NONE
	}


	if stats.LaneIdx < leastIndex {
		return LaneOrder{Lane: "Right", SwitchIdx: switch_idx}
	}
	if stats.LaneIdx > leastIndex {
		return LaneOrder{Lane: "Left", SwitchIdx: switch_idx}
	}
	return SWITCH_NONE
}
