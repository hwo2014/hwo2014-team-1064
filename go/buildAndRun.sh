#!/bin/bash

USER=`whoami`

mkdir -p go
export GOPATH=$GOPATH:$PWD

go test touk/... -test.v
if [ "$?" != "0" ]; then
    echo "ERROR: all test not passed"
    exit 1
fi

go build -o touk_bot.bin touk

if [ "$?" != "0" ]; then
    echo "Build failed"
    exit
fi

BOT_NAME="TOUK-go-$USER"
./touk_bot.bin -h hakkinen.helloworldopen.com -p 8091 -n "$BOT_NAME" -k pbmDwxN/bwVA4Q $@
