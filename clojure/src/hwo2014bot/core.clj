(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]]
        [clojure.pprint :only [print-table]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn count-switch [lap piece inPieceDistance]
  (cond
   (and (== piece 2) (<= inPieceDistance 10)) {:switch true :dir "Right"}
   (and (== piece 6) (<= inPieceDistance 10)) {:switch true :dir "Left"}
   (and (== piece 17) (<= inPieceDistance 10)) {:switch true :dir "Right"}
   :else {:switch false}))

(defn count-throttle [lap piece inPieceDistance]
  (cond
   (contains? (set [6 7 8 35 36 37 38 39 1 2]) piece) 1.0
   (== piece 3) 0.35
   (and (== piece 4) (<= inPieceDistance 50)) 0.35
   (and (== piece 4) (> inPieceDistance 50)) 0.35
   (and (== piece 5) (<= inPieceDistance 30)) 0.25
   (and (== piece 5) (> inPieceDistance 30)) 0.25
   ;(== piece 6) 1
   ;(== piece 8) 0.75
   ;(== piece 15) 0.5
   ;(contains? (set [16 24]) piece) 0.3
   ;(== piece 21) 0.5
   ;(== piece 28) 0.5
   ;(and (== piece 33) (> inPieceDistance 40)) 0.75
   ;(== piece 34) 0.77
   :else 0.65))

(defmulti handle-msg :msgType)

(defn print-track [track]
  (print (map-indexed #(assoc %2 :index %1) track)))

(def throttle (atom 0))

(defmethod handle-msg "carPositions" [msg]
  (let [car (first (:data msg))
        piece (:piecePosition car)
        inPieceDistance (:inPieceDistance piece)
        index (:pieceIndex piece)
        tick (:gameTick msg)
        lap (:lap piece)
        new-throttle (count-throttle lap index inPieceDistance)
        switch (count-switch lap index inPieceDistance)
        old-throttle (deref throttle)]

    ;;(println index inPieceDistance tick new-throttle switch)

    (when (not= old-throttle new-throttle)
      (println index "throttle" new-throttle)
      (reset! throttle new-throttle))
    (when (:switch switch) (println index "switching" (:dir switch)))

    (if (:switch switch)
      {:msgType "switchLane" :data (:dir switch)}
      {:msgType "throttle" :data new-throttle})))

(defmethod handle-msg "gameInit" [msg]
  (let [pieces (:pieces (:track (:race (:data msg))))]
    (print-track pieces))
  {:msgType "ping" :data "ping"})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "gameInit" (println "Got track")
    "lapFinished" (println "Lap finished")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
